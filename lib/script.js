'use strict';

/**
 * MNP Transactions (business logic of MNP blockchain solution)
 */

/**
 * Transaction which initiates the process. Creates porting case and notifies the donating operator.
 * @param {com.kimable.mnp.InitiatePortingProcess} initiatePortingProcess
 * @transaction
 */
function onInitiatePortingProcess(initiatePortingProcess) {

    var msisdn = initiatePortingProcess.msisdn;
    var subscriberId = initiatePortingProcess.subscriberId;

    return getAssetRegistry('com.kimable.mnp.Service') // find the service
        .then(function(ar) {
            var svc = ar.get(msisdn);
            // we could perform additional checks here: 
      		// - veryfing ownership of the service (if the subscriber matches)
      		// - veryfing the service type
            return svc;
        })
        .then(function(service) {            

            const factory = getFactory();
            // create porting case
            var uniqueCaseId = 'uniqeCaseId' + (new Date().getTime()) + Math.random();
            const portingCase = factory.newResource('com.kimable.mnp', 'PortingCase', uniqueCaseId);

            portingCase.portedMsisdn = factory.newRelationship('com.kimable.mnp', 'Service', service.msisdn);
            portingCase.newCarrier = factory.newRelationship('com.kimable.mnp', 'Carrier', initiatePortingProcess.newCarrier.mccmnc);
      
            return getAssetRegistry('com.kimable.mnp.PortingCase')
                .then(function(caseAr) {
              		// store new porting case in the registry
                    caseAr.add(portingCase);
                    return portingCase;
                });
        })
        .then(function(portingCase) {
            console.log("Notifing the donating");
            var event = getFactory().newEvent('com.kimable.mnp', 'InitiatePortingEvent');
            event.portingCase = portingCase;
            emit(event);
            return portingCase;
        })    
}